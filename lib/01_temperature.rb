def ftoc(temp_in_fahrenheit)
  (temp_in_fahrenheit - 32) * (5.0 / 9)
end

def ctof(temp_in_celsius)
  (temp_in_celsius * (9.0 / 5)) + 32
end
