def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, num=2)
  result_string = string
  (num - 1).times {result_string += " #{string}"}
  result_string
end

def start_of_word(word, num_of_letters)
  word[0, num_of_letters]
end

def first_word(string)
  string.split[0]
end

def article?(word)
  %w[a an and the of in on into onto over above for with
     by from to toward away].include?(word)
end

def titleize(string)
  string.split.reduce("") do |title, word|
    if title == ""
      title << word.capitalize
    elsif article?(word)
      title << " #{word}"
    else
      title << " #{word.capitalize}"
    end
    title
  end
end
