def translate(string)
  words = string.split
  translated_words = words.map do |word|
    if capitalized?(word)
      translate_word(word).capitalize
    else
      translate_word(word)
    end
  end

  translated_words.join(' ')
end

def translate_word(word)
  letters = word.downcase.split('')

  loop do
    if "aeiou".include?(letters[0]) &&
       (letters[0] != "u" || letters[-1] != "q")
      return "#{letters.join}ay"
    end

    letters.rotate!
  end
end

def capitalized?(word)
  word == word.capitalize
end
