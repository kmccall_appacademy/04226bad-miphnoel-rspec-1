def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(array)
  array.reduce(0, :+)
end

def multiply(array)
  array.empty? ? 0 : array.reduce(:*)
end

def power(base, exponent)
  return 1 if exponent.zero?

  if base.zero?
    return exponent < 0 ? "undefined" : 0
  end

  base ** exponent
end

def factorial(num)
  return 0 if num.zero?

  (1..num).reduce(:*)
end
